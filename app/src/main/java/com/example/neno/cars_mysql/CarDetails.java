package com.example.neno.cars_mysql;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by neno on 12.11.2015..
 */
public class CarDetails extends Activity {
   String key;  //STRINGOVE STAVIT GLOBALNO
    int value;


    //php skripta za post metodu -insert i delete
    //private static final String url_update = "http://10.0.3.2:8080/update_product1.php";//emulator
     private static final String url_update = "http://192.168.1.3:8080/update_product1.php";   //MOBITEL


    private static final String TAG_SUCCESS = "success";
    private static final String REG = "registracija";

    private static final String TAG_NAME = "marka";  //poveznica sa arg u php skripti
    private static final String TAG_MODEL = "model";
    private static final String TAG_STETE = "stete";
    private static final String TAG_KM = "kilometraza";
    private static final String TAG_TANK = "tank";


    JSONParser jsonParser = new JSONParser();

    private ProgressDialog pDialog;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cardetails);
        TextView model = (TextView) findViewById(R.id.model);
        TextView marka = (TextView) findViewById(R.id.marka);
        TextView reg = (TextView) findViewById(R.id.reg);
        TextView stanjeTanka = (TextView) findViewById(R.id.stanjeTanka);
        TextView stete = (TextView) findViewById(R.id.stete);
        TextView km = (TextView) findViewById(R.id.km);
        Button daj = (Button) findViewById(R.id.daj);


        //ImageView imageView =(ImageView) findViewById(R.id.imageView);


        final Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int position = extras.getInt("position");
            marka.setText("   " + (DataStorage.cars[position].marka).toString());
            model.setText(" " + (DataStorage.cars[position].model).toString());
            reg.setText((DataStorage.cars[position].registracija).toString());
            stanjeTanka.setText("Stanje tanka: " + (DataStorage.cars[position].stanjeTanka).toString() + "%");
            stete.setText("Stete: " + (DataStorage.cars[position].stete).toString());
            km.setText((DataStorage.cars[position].km).toString() + " km");
            //UrlImageViewHelper.setUrlDrawable(imageView, DataStorage.cars[position].img);

        }

        Log.d("ispis", marka.toString());

        final MyPreferences prefs = new MyPreferences(this);
        key="poz";
        value=extras.getInt("position");
        prefs.setInt(key,value);
        String val=String.valueOf(value);
        Log.d("shared",val);

        daj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = extras.getInt("position");

                new SaveProductDetails().execute();
            }
        });


    }


    class SaveProductDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CarDetails.this);
            pDialog.setMessage("Saving ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            //EditText regNova = (EditText) findViewById(R.id.regNova);
            //EditText markaNova = (EditText) findViewById(R.id.markaNova);
            //String poz=regNova.getText().toString();
            //Integer p=(Integer) Integer.parseInt(poz);
            // getting updated data from EditTexts


        //vrijednosti ne smiju biti null
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(REG,(DataStorage.cars[value].registracija).toString()));
            params.add(new BasicNameValuePair(TAG_NAME, (DataStorage.cars[value].marka).toString()));
            params.add(new BasicNameValuePair(TAG_MODEL, (DataStorage.cars[value].model).toString()));
            params.add(new BasicNameValuePair(TAG_KM, (DataStorage.cars[value].km).toString()));
            params.add(new BasicNameValuePair(TAG_STETE, (DataStorage.cars[value].stete).toString()));
            params.add(new BasicNameValuePair(TAG_TANK, (DataStorage.cars[value].stanjeTanka).toString()));
            // check json success tag
            JSONObject json = jsonParser.makeHttpRequest(url_update,
                    "POST", params);

            // check json success tag
            try {
                Log.d("try", "tryyyy");
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    Log.d("response", "uspjeh POST-A");
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            Intent intent = new Intent(getApplicationContext(),Pocetna.class);
            startActivity(intent);
            pDialog.dismiss();
        }




    }

}


