package com.example.neno.cars_mysql;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {

    private AsyncHttpClient httpClient = new AsyncHttpClient();
    private Gson mGson = new Gson();
    private ListView listView;


    private CustomAdapter adapter;

    //private String URL = "http://10.0.3.2:3000/contact";   //EMULATOR
   // private String URL = "http://192.168.1.3:3000/contact"; //MOBITEL
   //private String urlphp = "http://10.0.3.2:8080/get_all_products.php";   //EMULATOR
    private String urlphp = "http://192.168.1.3:8080/get_all_products.php"; //MOBITEL

    private TextView tv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);//kreiraj novi obj tipa lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(),CarDetails.class);
                intent.putExtra("position",position); //proslijedi position u drugi activity
                startActivity(intent);


            }


        });

        httpClient.get(urlphp, new JsonHttpResponseHandler() { //get zahtjev,dobavi JSON sa URL



            @Override
            public void onSuccess(JSONArray response) {
                Log.d("pre_RESPONSE", response.toString());
                DataStorage.cars = mGson.fromJson(response.toString(),Car[].class);  //JSON konvertaj u string, napravi Car objekte
                Log.d("HTTP_RESPONSE", "KONVERTAN GSON");

                adapter = new CustomAdapter(getApplicationContext());//adapter u trenutnom aktivitiju

                listView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Throwable error) {
                Toast.makeText(getApplicationContext(), "error konekcije", Toast.LENGTH_LONG).show();
            }


        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}


