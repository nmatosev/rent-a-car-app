package com.example.neno.cars_mysql;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by neno on 18.11.2015..
 */
public class MyPreferences {
    String PREFS_NAME = "MyPreferences";
    SharedPreferences sharedPreferencesInstance;

    public MyPreferences(Context context) {
        sharedPreferencesInstance =context.getSharedPreferences(PREFS_NAME,0);
    }

    public void setString(String key, String value) {
        sharedPreferencesInstance.edit().putString(key,value).commit();
    }

    public void setInt(String key, Integer value) {
        sharedPreferencesInstance.edit().putInt(key,value).commit();
    }


    public String getString(String key) {
        return sharedPreferencesInstance.getString(key, "");//2. arg vrati u slucaju da ne postoji
    }
    public Integer getInt(String key) {
        return sharedPreferencesInstance.getInt(key, 0);
    }
}
