package com.example.neno.cars_mysql;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by neno on 14.11.2015..
 */
public class RentedDetails extends Activity {

    String key;  //STRINGOVE STAVIT GLOBALNO
    int value;
    //int progress_value;

    //php skripta za post u tablicu dostupnih auta,brisanje iz trenutne
    //private static final String url_update = "http://10.0.3.2:8080/update_product2.php";//EMULATOR
    private static final String url_update = "http://192.168.1.3:8080/update_product2.php";   //MOBITEL

    private static final String TAG_SUCCESS = "success";
    private static final String REG = "registracija";

    private static final String TAG_NAME = "marka";  //poveznica sa arg u php
    private static final String TAG_MODEL = "model";
    private static final String TAG_STETE = "stete";
    private static final String TAG_KM = "kilometraza";
    private static final String TAG_TANK = "tank";


    JSONParser jsonParser = new JSONParser();

    private ProgressDialog pDialog;

    public static SeekBar seek_bar;
    private static TextView gorivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.renteddetails);
        TextView model = (TextView) findViewById(R.id.model);
        TextView marka = (TextView) findViewById(R.id.marka);
        TextView reg = (TextView) findViewById(R.id.reg);
        TextView stanjeTanka = (TextView) findViewById(R.id.stanjeTanka);
        TextView stete = (TextView) findViewById(R.id.stete);
        TextView km = (TextView) findViewById(R.id.km);

        Button vrati = (Button) findViewById(R.id.vrati);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int position = extras.getInt("position");
            marka.setText("   " + (DataStorage.cars[position].marka).toString());
            model.setText(" " + (DataStorage.cars[position].model).toString());
            reg.setText((DataStorage.cars[position].registracija).toString());
            km.setText("Izdan na:" + (DataStorage.cars[position].km).toString() + " km");
            stete.setText("Stete: " + (DataStorage.cars[position].stete).toString());
            stanjeTanka.setText("Izdan na: " + (DataStorage.cars[position].stanjeTanka).toString() + "%");
            //UrlImageViewHelper.setUrlDrawable(imageView, DataStorage.cars[position].img);
        }

        setSeekBar();

        Log.d("ispis", marka.toString());

        final MyPreferences prefs = new MyPreferences(this);
        key = "poz";
        value = extras.getInt("position");
        prefs.setInt(key, value);
        String val = String.valueOf(value);
        Log.d("shared", val);

        vrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SaveProductDetails().execute();
            }
        });


    }

    public void setSeekBar() {
        seek_bar = (SeekBar) findViewById(R.id.seekBar);
        gorivo = (TextView) findViewById(R.id.gorivo);
        gorivo.setText("Novo stanje tanka " + seek_bar.getProgress() + "/" + seek_bar.getMax());
        //final int progress_value;
        Integer progress_value;

        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            Integer progress_value;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value = progress;
                gorivo.setText("Novo stanje tanka " + progress + "/" + seek_bar.getMax());


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                gorivo.setText("Stanje tanka " + progress_value + "/" + seek_bar.getMax());
                Log.d("gorivo",(progress_value.toString()));
            }
        });

    }


    class SaveProductDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RentedDetails.this);
            pDialog.setMessage("Saving ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            String tank=String.valueOf(seek_bar.getProgress());
            EditText km_novi = (EditText) findViewById(R.id.km_novi);
            //vrijednosti ne smiju biti null,app puca
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(REG, (DataStorage.cars[value].registracija).toString()));
            params.add(new BasicNameValuePair(TAG_NAME, (DataStorage.cars[value].marka).toString()));
            params.add(new BasicNameValuePair(TAG_MODEL, (DataStorage.cars[value].model).toString()));
            params.add(new BasicNameValuePair(TAG_KM, km_novi.getText().toString()));
            params.add(new BasicNameValuePair(TAG_STETE,(DataStorage.cars[value].stete).toString()));
            params.add(new BasicNameValuePair(TAG_TANK,tank));
            // check json success tag
            JSONObject json = jsonParser.makeHttpRequest(url_update,
                    "POST", params);

            // check json success tag
            try {
                Log.d("try", "tryyyy");
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    Log.d("response", "uspjeh POST-A");
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();
            Intent intent = new Intent(getApplicationContext(),Pocetna.class);
            startActivity(intent);



        }

    }
}
