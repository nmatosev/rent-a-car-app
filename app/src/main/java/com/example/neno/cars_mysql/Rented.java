package com.example.neno.cars_mysql;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;

/**
 * Created by neno on 14.11.2015..
 */



public class Rented extends ActionBarActivity {

    private AsyncHttpClient httpClient = new AsyncHttpClient();
    private Gson mGson = new Gson();
    private ListView listView;


    private RentedAdapter adapter;

    //private String URL = "http://10.0.3.2:3000/contact";   //EMULATOR
   // private String URL = "http://192.168.1.3:3000/contact"; //MOBITEL
    //private String urlphp = "http://10.0.3.2:8080/get_all_rented.php";   //EMULATOR
    private String urlphp = "http://192.168.1.3:8080/get_all_rented.php";   //MOBITEL

    private TextView tv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rented);

        listView = (ListView) findViewById(R.id.listView);//kreiraj novi obj tipa lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(),RentedDetails.class);
                intent.putExtra("position",position); //proslijedi position u drugi activity
                startActivity(intent);


            }


        });

        httpClient.get(urlphp, new JsonHttpResponseHandler() { //get zahtjev,dobavi JSON sa URL
            @Override
            public void onSuccess(JSONArray response) {
                DataStorage.cars = mGson.fromJson(response.toString(),Car[].class);  //JSON konvertaj u string, napravi Car objekte
                adapter = new RentedAdapter(getApplicationContext());//adapter u trenutnom aktivitiju

                listView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Throwable error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }


        });

    }
}
