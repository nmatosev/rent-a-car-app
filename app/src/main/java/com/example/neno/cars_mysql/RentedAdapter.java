package com.example.neno.cars_mysql;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by neno on 10.11.2015..
 */
public class RentedAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    public RentedAdapter(Context context)
    {

        mContext = context;
        mInflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
       return DataStorage.cars.length;
    }

    @Override
    public Object getItem(int position) {
        //return carItem.get(position);
        return  null;


    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = mInflater.inflate(R.layout.item_rented,parent,false);


        //ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnailImg);
        TextView reg = (TextView) convertView.findViewById(R.id.reg);
        TextView marka = (TextView) convertView.findViewById(R.id.marka);
        TextView model = (TextView) convertView.findViewById(R.id.model);
        TextView stanjeTanka = (TextView) convertView.findViewById(R.id.stanjeTanka);

       // if(((DataStorage.cars[position].dostupan)==0)) {
       //     Log.d("broj", "view number " + DataStorage.cars[position].dostupan);
            reg.setText((DataStorage.cars[position].registracija).toString());
            marka.setText(" " + (DataStorage.cars[position].marka).toString()); //podatak obavezno u string inače puca APP
            //Log.d("aaaaaaaaa", "manufacturer " + DataStorage.cars[position].manufacturer);//pozicija je int postavljen na 0,ink skrolon
            model.setText(" " + (DataStorage.cars[position].model).toString()); //pristupanje datstorageu na poziciju objekta
       //     stanjeTanka.setText("Tank: " + (DataStorage.cars[position].stanjeTanka).toString() + "%");
            //UrlImageViewHelper.setUrlDrawable(thumbnail,DataStorage.cars[position].thumbnail);
        //}





        return convertView;
    }
}
